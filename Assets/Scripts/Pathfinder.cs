﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pathfinder
{
    private class Node
    {
        public int X; 
        public int Y; 
        public int DistanceToStart;
        public Node Previous;
        public Node (int x, int y, Node previous = null)
        {
            Previous = previous;
            X = x;
            Y = y;
            DistanceToStart = 0;
        }
    }

    public int FindPath(int startX, int startY, int targetX, int targetY, int[,] maze, int mazeSize, Stack<Vector2> pathPoints)
    {
        if (startX == targetX && startY == targetY)
            return 0;

        if (maze[targetX, targetY] == 0)
            return -1;
        
        Vector2[] direction = { new Vector2(1, 0), new Vector2(0, 1), new Vector2(-1, 0), new Vector2(0, -1) };
        Node current = null;
        List<Node> openMap = new List<Node>();  
        List<Node> closedSet = new List<Node>();
        
        openMap.Add(new Node(startX, startY));

        while (openMap.Count != 0)
        {
            current = openMap.First();
            
            foreach (var node in openMap)
                if (node.DistanceToStart <= current.DistanceToStart)
                    current = node;

            if (current.X == targetX && current.Y == targetY)
                break;

            closedSet.Add(current);
            openMap.Remove(current);

            for (int i = 0; i < direction.Length; i++)
            {
                var nextX = (int) (current.X + direction[i].x);
                var nextY = (int) (current.Y + direction[i].y);
                
                if (nextX < 0 || nextY < 0)
                    continue;
                
                if (nextX >= mazeSize || nextY >= mazeSize)
                    continue;

                if (maze[nextX, nextY] == 0)
                    continue;

                foreach (var node in closedSet)
                    if (node.X == nextX && node.Y == nextY)
                        continue;

                int nextDistanceToStart = current.DistanceToStart + 1;

                var successor = openMap.Find(node => node.X == nextX && node.Y == nextY);

                if (successor == default) 
                {
                    var node = new Node(nextX, nextY, current);
                    node.DistanceToStart = nextDistanceToStart;
                    openMap.Add(node);
                }
                else if (nextDistanceToStart < successor.DistanceToStart) 
                {
                    successor.Previous = current;
                    successor.DistanceToStart = nextDistanceToStart;
                }
            }
        }
        
        int distance = 0;
    
        if (current.X != targetX && current.Y != targetY)
            distance = -1;
        else
        {
            while (current.Previous != null) 
            {
                pathPoints.Push(new Vector2(current.X, current.Y));
                current = current.Previous;

                distance++;
            }
        }

        return distance;
    }
}