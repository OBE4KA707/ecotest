﻿using UnityEngine;

public class MazeVisualizer : MonoBehaviour
{
    [SerializeField] private GameObject _cell;
    [SerializeField] private GameObject _wall;
    
    public void Visualize(int[,] maze, int mazeSize)
    {
        for (int i = 0; i < mazeSize; i++)
        {
            for (int j = 0; j < mazeSize; j++)
            {
                var prefab = maze[i, j] == 1 ? _cell : _wall;
                var go = Instantiate(prefab);
                go.name = i + " " + j;
                go.transform.position = new Vector3(i, j);
            }
        }
    }
}