﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private Vector3 _mouseHitPosition;
    private const float Distance = 1000f;

    public Action<Vector3> OnMouseHit;
    
    private void FixedUpdate () 
    {    
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Distance))
            {
                var fixedHitPoint = hit.point + new Vector3(0.5f, 0.5f);
                _mouseHitPosition = CastToInt(fixedHitPoint);
                OnMouseHit(_mouseHitPosition);
            }
        }    
    }

    private Vector3 CastToInt(Vector3 floatPoint)
    {
        var x = (int) floatPoint.x;
        var y = (int) floatPoint.y;
        var z = (int) floatPoint.z;
        
        return new Vector3(x, y,z );
    }
}