﻿using System.Collections.Generic;
using Random = UnityEngine.Random;

public class MazeGenerator
{
    public class Node 
    {
        public readonly int x;
        public readonly int y;

        public Node(int x, int y) 
        {
            this.x = x;
            this.y = y;
        }
    }

    public int[,] Maze => _maze;

    private Stack<Node> stack = new Stack<Node>();
    private int[,] _maze;
    private int _dimension;

    public MazeGenerator(int mazeSize)
    {
        _maze = new int[mazeSize, mazeSize];
        _dimension = mazeSize;
        GenerateMaze();
    }

    public void GenerateMaze()
    {
        stack.Push(new Node(0,0));
        
        while (stack.Count != 0) 
        {
            Node next = stack.Pop();
            
            if (ValidNextNode(next)) 
            {
                _maze[next.y, next.x] = 1;
                List<Node> neighbors = FindNeighbors(next);
                RandomlyAddNodesToStack(neighbors);
            }
        }
    }

    private bool ValidNextNode(Node node) 
    {
        int numNeighboringOnes = 0;
        
        for (int y = node.y-1; y < node.y+2; y++) 
            for (int x = node.x-1; x < node.x+2; x++) 
                if (PointOnGrid(x, y) && PointNotNode(node, x, y) && _maze[y, x] == 1) 
                    numNeighboringOnes++;

        return (numNeighboringOnes < 3) && _maze[node.y, node.x] != 1;
    }

    private void RandomlyAddNodesToStack(List<Node> nodes) 
    {
        int targetIndex;
        
        while (nodes.Count != 0)
        {
            targetIndex = Random.Range(0, nodes.Count - 1);
            var node = nodes[targetIndex];
            stack.Push(node);
            nodes.RemoveAt(targetIndex);
        }
    }

    private List<Node> FindNeighbors(Node node) 
    {
        List<Node> neighbors = new List<Node>();
        
        for (int y = node.y-1; y < node.y+2; y++) 
            for (int x = node.x-1; x < node.x+2; x++) 
                if (PointOnGrid(x, y) && PointNotCorner(node, x, y) && PointNotNode(node, x, y)) 
                    neighbors.Add(new Node(x, y));
                
        return neighbors;
    }

    private bool PointOnGrid(int x, int y) 
    {
        return x >= 0 && y >= 0 && x < _dimension && y < _dimension;
    }

    private bool PointNotCorner(Node node, int x, int y) 
    {
        return (x == node.x || y == node.y);
    }
    
    private bool PointNotNode(Node node, int x, int y) 
    {
        return !(x == node.x && y == node.y);
    }
}
