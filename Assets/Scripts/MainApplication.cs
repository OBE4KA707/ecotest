﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MainApplication : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private InputController _inputController;
    [SerializeField] private MazeVisualizer _mazeVisualizer;
    [SerializeField] private int _mazeSize = 4;
    private MazeGenerator _mazeGenerator;
    private Pathfinder _pathfinder;
    
    private void Start()
    {
        SetCameraPosition();
        InitMazeGenerator();
        InitPathFinder();
        VisualizeMaze();
        Subscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void InitMazeGenerator()
    {
        _mazeGenerator = new MazeGenerator(_mazeSize);
        _mazeGenerator.Maze[0, 0] = 1;
    }

    private void InitPathFinder()
    {
        _pathfinder = new Pathfinder();
    }

    private void VisualizeMaze()
    {
        _mazeVisualizer.Visualize(_mazeGenerator.Maze, _mazeSize);
    }

    private void Subscribe()
    {
        _inputController.OnMouseHit += OnMouseHit;
    }
    
    private void Unsubscribe()
    {
        _inputController.OnMouseHit -= OnMouseHit;
    }

    private void OnMouseHit(Vector3 hitPosition)
    {
        var pathPoints = new Stack<Vector2>();
        var dist = _pathfinder.FindPath((int)_player.PlayerPosition.x, (int)_player.PlayerPosition.y, (int)hitPosition.x, (int)hitPosition.y,  _mazeGenerator.Maze, _mazeSize, pathPoints);
        _player.StartPlayerMovement(pathPoints);
    }

    private void SetCameraPosition()
    {
        var camera = Camera.main;
        camera.transform.position = new Vector3(_mazeSize / 2f - 0.5f, _mazeSize / 2f - 0.5f, -_mazeSize);
        camera.orthographicSize = _mazeSize / 2f;
    }
}