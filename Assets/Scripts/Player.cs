﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private float _waitTime = 0.2f;

    public Vector3 PlayerPosition = Vector3.zero;
    private GameObject _playerGO;
    private bool _isPlayerBusy = false;
    
    private void Start()
    {
        SpawnPlayer();
    }

    public void StartPlayerMovement(Stack<Vector2> pathPoints)
    {
        if (!_isPlayerBusy)
            StartCoroutine(WaitAndMove(_waitTime, pathPoints));
    }
    
    private IEnumerator WaitAndMove(float waitTime, Stack<Vector2> pathPoints)
    {
        while (pathPoints.Count != 0)
        {
            yield return new WaitForSeconds(waitTime);
            var point = pathPoints.Pop();
            MovePlayer(point);
            _isPlayerBusy = true;
        }

        _isPlayerBusy = false;
    }

    private void MovePlayer(Vector2 toPosition)
    {
        PlayerPosition = toPosition;
        _playerGO.transform.position = PlayerPosition;
    }

    private void SpawnPlayer()
    {
        _playerGO = Instantiate(_prefab);
        _playerGO.transform.position = PlayerPosition;
    }
}
